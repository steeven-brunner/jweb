package Controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import Entities.User;
import ORM.DBFunc;

/**
 * Created by Steeven on 21/12/2015.
 */

public class Register extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher("jweb/Eshopper/login.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, String> error = new HashMap<String, String>();

        String email = request.getParameter("email");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String news = request.getParameter("news");
        boolean newsletter;

        boolean admin = true;//Todo: set en fonction de la valeur dans la DB

        if (news == null)
            newsletter = false;
        else
            newsletter = true;

        try {
            validPassword(password);
        }
        catch (Exception e) {
            error.put("password", e.getMessage());
        }

        try {
            validEmail(email);
        }
        catch (Exception e) {
            error.put("email", e.getMessage());
        }

        if (error.isEmpty()) {
            request.setAttribute("sucess", "ok");

            //User user = new User(email, pseudo, UserRole.USER, newsletter, "test salt", password);
            User user = new User(username, password, email, newsletter, admin); //Todo: Voir ce qu'on fait de l'ID

            DBFunc db = new DBFunc();

            db.ConnectBase();
            db.add_user(user);

        } else {
            request.setAttribute("sucess", "ko");
            error.put("gen", "error request");
        }

        request.setAttribute("error", error);

        this.getServletContext().getRequestDispatcher("jweb/Eshopper/login.jsp").forward(request, response); //Todo:Check le repo indiqué
    }

    private void validPassword(String password) throws Exception
    {
        if (password == null || password.trim().length() == 0 || password.length() <= 6)
            throw new Exception("Password must contain at least 6 characters");

    }

    private void validPseudo(String pseudo) throws Exception
    {
        if (pseudo == null || pseudo.trim().length() == 0)
            throw new Exception("Met un pseudo");
    }


    private void validEmail(String email) throws Exception
    {
        if (email != null && email.trim().length() != 0)
        {
            if (email.matches("^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$") == false)
            {
                throw new Exception("Please may sure your email is valid !");
            }
        }
        else
        {
            throw new Exception("Please may sure your email is valid !");
        }
    }
}