package ORM;

import com.sun.org.apache.xpath.internal.operations.Bool;
import Entities.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Steeven on 21/12/2015.
 */
public class DBFunc {

    private Connection  conn;
    private String      url;
    private String      user;
    private String      password;
    private String      querry;

    public      DBFunc()
    {
        url = "jdbc:mysql://localhost:3306/jweb";
        user = "root";
        password = "";
        conn = null;
        querry = "";
    }

    public Boolean ConnectBase() {

        try {

            conn = DriverManager.getConnection(url, user, password);
            if (conn != null) {
                System.out.println("Connect to data base succeed !\n");
            }
            return (true);

        } catch (SQLException ex) {
            System.out.println("An error occurred. Maybe user/password is invalid");
            ex.printStackTrace();
            return (false);
        }
        //conn.close(); Todo:Voir quand je close
    }

    public ResultSet execute_SELECT_query(String query) throws SQLException
    {
        ResultSet   res;

        try
        {
            Statement stmt = conn.createStatement();
            res = stmt.executeQuery(query);
        }
        catch (SQLException e)
        {
            System.out.print(e.getMessage());
            throw (e);
        }
        return (res);
    }

    public boolean     execute_INSERT_query(String query)
    {
        try
        {
            Statement stmt = conn.createStatement() ;
            stmt.executeUpdate(query);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return (false);
        }
        return (true);
    }

    public boolean     execute_DELETE_query(String query)
    {
        try
        {
            Statement stmt = conn.createStatement() ;
            stmt.executeUpdate(query);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return (false);
        }
        return (true);
    }

    public boolean      add_user(User user)
    {
        String          query = "INSERT INTO users (username, password, email, newsletter, admin) VALUES('";

        query += user.getUsername() + "', '" + user.getPassword() + "', '" + user.getEmail() + "', " + user.getNewsletter() + ", '" + user.getAdmin();
        query += "') ;";
        return (execute_INSERT_query(query));
    }


    public boolean      user_already_exist(String user_mail)
    {
        Iterator<User> list = getUserList().iterator();
        User              user;

        if (list != null)
        {
            while (list.hasNext())
            {
                user = list.next();
                if (user.getEmail().compareTo(user_mail) == 0)
                    return (true);
            }
            return (false);
        }
        return (true);
    }

    public User     getUser(String email, String password)
    {
        String      query = "SELECT * FROM users WHERE email = '" + email + "' AND password = '" + password + "';";
        ResultSet   res;

        try
        {
            res = execute_SELECT_query(query);
            while (res.next())
            {
                if (res.getString("email").equals(email) && res.getString("password").equals(password))
                {
                    return (new User(res.getInt("id"),
                            res.getString("username"),
                            res.getString("password"),
                            res.getString("email"),
                            res.getBoolean("newsletter"),
                            res.getBoolean("admin")));
                }
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return (null);
    }

    public User     getSpecificUser(int id)
    {
        String      query = "SELECT * FROM users WHERE id = " + id + " ;";
        ResultSet   res;

        try
        {
            res = execute_SELECT_query(query);
            while (res.next())
            {
                if (res.getInt("id") == id)
                {
                    return (new User(res.getInt("id"),
                            res.getString("username"),
                            res.getString("password"),
                            res.getString("email"),
                            res.getBoolean("newsletter"),
                            res.getBoolean("admin")));
                }
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return (null);
    }

    public List<User>   getUserList()
    {
        List<User> list = new ArrayList<User>();
        String  query = "SELECT email, id, username from users;";
        ResultSet res;

        try
        {
            res = execute_SELECT_query(query);
            while (res.next())
            {
                list.add(new User(res.getString("username"), res.getString("email"), res.getInt("id")));
            }
            return (list);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return (null);
    }
}
