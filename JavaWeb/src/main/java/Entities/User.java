package Entities;

/**
 * Created by Steeven on 22/12/2015.
 */
public class User {

    private int         id;
    private String      username;
    private String      password;
    private String      email;
    private Boolean     newsletter;
    private Boolean     admin;

    public User()
    {
        this.id = 0;
        this.username = "";
        this.password = "";
        this.email = "";
        this.newsletter = false;
        this.admin = false;
    }

    public User(int id, String username, String password, String email,
                Boolean newsletter, Boolean admin)
    {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.newsletter = newsletter;
        this.admin = admin;
    }

    public User(String username, String password, String email,
                Boolean newsletter, Boolean admin)
    {
        this.id = 0;
        this.username = username;
        this.password = password;
        this.email = email;
        this.newsletter = newsletter;
        this.admin = admin;
    }

    public User(String username, String email, int id)
    {
        this.id = id;
        this.username = username;
        this.password = "";
        this.email = email;
        this.newsletter = false;
        this.admin = false;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return (this.id);
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String   getUsername()
    {
        return (this.username);
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String   getPassword()
    {
        return (this.password);
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getEmail()
    {
        return (this.email);
    }

    public void setNewsletter(Boolean newsletter)
    {
        this.newsletter = newsletter;
    }

    public Boolean getNewsletter()
    {
        return (this.newsletter);
    }

    public void setAdmin(Boolean admin)
    {
        this.admin = admin;
    }

    public Boolean getAdmin()
    {
        return (this.admin);
    }

}
